


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct casilla{
	char etiqueta;
	char tipo;
	int nminas;
};
typedef struct casilla tipocasilla;

tipocasilla **iniciarmatrix(int filas, int column){

	tipocasilla **matrix = (tipocasilla **) malloc (filas*sizeof(tipocasilla *));

	for (int i = 0; i < filas; i++){
		matrix[i]=(tipocasilla *) malloc (column*sizeof(tipocasilla));
	}
	return matrix;
}

void llenarmatrix(tipocasilla **matrix, int filas, int column, int minas){

	for (int i = 0; i < filas; i++){
		for (int j = 0; j < column; j++){
			matrix[i][j].etiqueta='.';
			matrix[i][j].tipo='c';
			matrix[i][j].nminas=minas;
		}
	}
}

void imprimir(int filas, int column, tipocasilla **matrix){

	for (int i = 0; i < filas; i++){
		for (int j = 0; j < column; j++){
			printf("%c\t",matrix[i][j].tipo);
			

		}
		printf("\n");
	}
	printf("\n");

	for (int i = 0; i < filas; i++){
		for (int j = 0; j < column; j++){
			printf("%d\t",matrix[i][j].nminas);

		}
		printf("\n");
	}
	printf("\n");

	for (int i = 0; i < filas; i++){
		for (int j = 0; j < column; j++){
			printf("%d\t",matrix[i][j].etiqueta);

		}
		printf("\n");
	}

}


int main(int argc, char *argv[]){
	int filas = atoi(argv[1]);
	int column = atoi(argv[2]);
	int minas = atoi(argv[3]);

	tipocasilla **matrix;
	matrix = iniciarmatrix(filas,column);
	llenarmatrix(matrix,filas,column,minas);
	imprimir(filas,column,matrix);


}