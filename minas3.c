


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct casilla{
	int etiqueta;
	char tipo;
	int nminas;
};
typedef struct casilla tipocasilla;

tipocasilla **iniciarmatrix(int filas, int column){

	tipocasilla **matrix = (tipocasilla **) malloc (filas*sizeof(tipocasilla *));

	for (int i = 0; i < filas; i++){
		matrix[i]=(tipocasilla *) malloc (column*sizeof(tipocasilla));
	}
	return matrix;
}

void llenarmatrix(tipocasilla **matrix, int filas, int column, int minas){

	for (int i = 0; i < filas; i++){
		for (int j = 0; j < column; j++){
			matrix[i][j].etiqueta=0;
			matrix[i][j].tipo='c';
			matrix[i][j].nminas=0;
		}
	}
}

void llenarminas(tipocasilla **matrix, int filas, int column, int minas){

	int posfil,poscol;
	srand(time(NULL));
	int n = 0;

	while (n<=minas){
		posfil=rand()%filas;
		poscol=rand()%filas;
		matrix[posfil][poscol].tipo='m';
		n++;
	}
}

void minascercanas(tipocasilla **matrix, int filas, int column, int minas){

	for (int i = 0; i < filas-1; i++){
			int n=0;
			if(matrix[i][0].tipo != 'm')
				if(matrix[i][1].tipo == 'm')
					n++;
				if(matrix[i+1][0].tipo == 'm')
					n++;
				if(matrix[i+1][1].tipo == 'm')
					n++;	
				matrix[i][0].nminas = n;
	}
	for (int i = 1; i < filas; i++){
			int n=0;
			if(matrix[i][0].tipo != 'm')
				if(matrix[i][1].tipo == 'm')
					n++;
				if(matrix[i-1][0].tipo == 'm')
					n++;
				if(matrix[i-1][1].tipo == 'm')
					n++;	
				matrix[i][0].nminas = n;
	}
	for (int i = 0; i < column-1; i++){
			int n=0;
			if(matrix[0][i].tipo != 'm')
				if(matrix[i][i].tipo == 'm')
					n++;
				if(matrix[0][i+1].tipo == 'm')
					n++;
				if(matrix[1][i+1].tipo == 'm')
					n++;	
				matrix[0][i].nminas = n;
	}
	for (int i = 1; i < column; i++){
			int n=0;
			if(matrix[0][i].tipo != 'm')
				if(matrix[i][i].tipo == 'm')
					n++;
				if(matrix[0][i-1].tipo == 'm')
					n++;
				if(matrix[1][i-1].tipo == 'm')
					n++;	
				matrix[0][i].nminas = n;
	}
	for (int i = 1; i < filas-1; i++){
			int n=0;
			int p=column;
			if(matrix[i][p].tipo != 'm')
				if(matrix[i][p-1].tipo == 'm')
					n++;
				if(matrix[i+1][p+1].tipo == 'm')
					n++;
				if(matrix[i+1][p].tipo == 'm')
					n++;
				if(matrix[i-1][p].tipo == 'm')
					n++;
				if(matrix[i-1][p-1].tipo == 'm')
					n++;
				matrix[i][p].nminas = n;			
	}
	for (int i = 1; i < filas-1; i++){
		for (int j = 1; j < column-1; j++){
			int n=0;
			if(matrix[i][j].tipo != 'm')
				if(matrix[i-1][j].tipo == 'm')
					n++;
				if(matrix[i][j-1].tipo == 'm')
					n++;
				if(matrix[i-1][j-1].tipo == 'm')
					n++;
				if(matrix[i+1][j].tipo == 'm')
					n++;
				if(matrix[i][j+1].tipo == 'm')
					n++;
				if(matrix[i+1][j+1].tipo == 'm')
					n++;
				if(matrix[i+1][j-1].tipo == 'm')
					n++;
				if(matrix[i-1][j+1].tipo == 'm')
					n++;
				matrix[i][j].nminas = n;
		}
	}
}

void abrir(int posx, int posy, tipocasilla **matrix){

	int *n = &matrix[posx][posy].nminas;
	matrix[posx][posy].etiqueta = n[0];
}

void marcador(int posx, int posy, tipocasilla **matrix){

	matrix[posx][posy].etiqueta = '$' ;
}

void imprimir(int filas, int column,tipocasilla **matrix){

	for (int i = 0; i < filas; i++){
		for (int j = 0; j < column; j++){
		printf("|%d|",matrix[i][j].etiqueta);			
		}
	printf("\n");
	}

}

void jugar(int filas, int column, tipocasilla **matrix){
	int f=0;
	int xpos;
	int ypos;
	char *comando;
	while(f==0){
		//scanf("%s, %d, %d",comando, &xpos, &ypos);
		scanf("%d, %d", &xpos, &ypos);
		//if (comando == "o")
			if(matrix[xpos][ypos].tipo =='m')
				f=1;
			if(matrix[xpos][ypos].tipo =='c')
				abrir(xpos,ypos,matrix);
				imprimir(filas,column,matrix);
				f=0;
		//if (comando == "f")
			//marcador(xpos,ypos,matrix);
			//f=0;
	}


}

int main(int argc, char *argv[]){
	int filas = atoi(argv[1]);
	int column = atoi(argv[2]);
	int minas = atoi(argv[3]);

	tipocasilla **matrix;
	matrix = iniciarmatrix(filas,column);
	llenarmatrix(matrix,filas,column,minas);
	llenarminas(matrix,filas,column,minas);
	minascercanas(matrix,filas,column,minas);
	jugar(filas,column,matrix);


}