


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct casilla{
	int etiqueta;
	char tipo;
	int nminas;
	int estado;
};
typedef struct casilla tipocasilla;

tipocasilla **iniciarmatrix(int filas, int column){

	tipocasilla **matrix = (tipocasilla **) malloc (filas*sizeof(tipocasilla *));

	for (int i = 0; i < filas; i++){
		matrix[i]=(tipocasilla *) malloc (column*sizeof(tipocasilla));
	}
	return matrix;
}

void llenarmatrix(tipocasilla **matrix, int filas, int column, int minas){

	for (int i = 0; i < filas; i++){
		for (int j = 0; j < column; j++){
			matrix[i][j].etiqueta='.';
			matrix[i][j].tipo='c';
			matrix[i][j].nminas=0;
			matrix[i][j].estado=0;
		}
	}
}

void llenarminas(tipocasilla **matrix, int filas, int column, int minas){

	int posfil,poscol;
	srand(time(NULL));
	int n = 0;
	while (n<=minas){
		posfil=rand()%filas;
		poscol=rand()%filas;
		matrix[posfil][poscol].tipo='m';
		n++;
	}
}

void minascercanas(tipocasilla **matrix, int filas, int column, int minas){

	for (int i = 0; i < filas-1; i++){
			int n=0;
			if(matrix[i][0].tipo != 'm')
				if(matrix[i][1].tipo == 'm')
					n++;
				if(matrix[i+1][0].tipo == 'm')
					n++;
				if(matrix[i+1][1].tipo == 'm')
					n++;	
				matrix[i][0].nminas = n;
	}
	for (int i = 1; i < filas; i++){
			int n=0;
			if(matrix[i][0].tipo != 'm')
				if(matrix[i][1].tipo == 'm')
					n++;
				if(matrix[i-1][0].tipo == 'm')
					n++;
				if(matrix[i-1][1].tipo == 'm')
					n++;	
				matrix[i][0].nminas = n;
	}
	for (int i = 0; i < column-1; i++){
			int n=0;
			if(matrix[0][i].tipo != 'm')
				if(matrix[i][i].tipo == 'm')
					n++;
				if(matrix[0][i+1].tipo == 'm')
					n++;
				if(matrix[1][i+1].tipo == 'm')
					n++;	
				matrix[0][i].nminas = n;
	}
	for (int i = 1; i < column; i++){
			int n=0;
			if(matrix[0][i].tipo != 'm')
				if(matrix[i][i].tipo == 'm')
					n++;
				if(matrix[0][i-1].tipo == 'm')
					n++;
				if(matrix[1][i-1].tipo == 'm')
					n++;	
				matrix[0][i].nminas = n;
	}
	for (int i = 1; i < filas-1; i++){
			int n=0;
			int p=column;
			if(matrix[i][p].tipo != 'm')
				if(matrix[i][p-1].tipo == 'm')
					n++;
				if(matrix[i+1][p+1].tipo == 'm')
					n++;
				if(matrix[i+1][p].tipo == 'm')
					n++;
				if(matrix[i-1][p].tipo == 'm')
					n++;
				if(matrix[i-1][p-1].tipo == 'm')
					n++;
				matrix[i][p].nminas = n;			
	}
	for (int i = 1; i < filas-1; i++){
		for (int j = 1; j < column-1; j++){
			int n=0;
			if(matrix[i][j].tipo != 'm')
				if(matrix[i-1][j].tipo == 'm')
					n++;
				if(matrix[i][j-1].tipo == 'm')
					n++;
				if(matrix[i-1][j-1].tipo == 'm')
					n++;
				if(matrix[i+1][j].tipo == 'm')
					n++;
				if(matrix[i][j+1].tipo == 'm')
					n++;
				if(matrix[i+1][j+1].tipo == 'm')
					n++;
				if(matrix[i+1][j-1].tipo == 'm')
					n++;
				if(matrix[i-1][j+1].tipo == 'm')
					n++;
				matrix[i][j].nminas = n;
		}
	}
}

void abrircercano(tipocasilla **matrix, int xpos, int ypos, int minas, int column, int filas){

	if(matrix[xpos][ypos].tipo != 'm'){
		if(xpos>0 && ypos>0){
				if(matrix[xpos][ypos-1].nminas == matrix[xpos][ypos].nminas)
					matrix[xpos][ypos-1].estado = 1;
				if(matrix[xpos-1][ypos].nminas == matrix[xpos][ypos].nminas)
					matrix[xpos-1][ypos].estado = 1;
				if(matrix[xpos-1][ypos-1].nminas == matrix[xpos][ypos].nminas)
					matrix[xpos-1][ypos-1].estado = 1;
		}
	}


}

void abrir(int xpos, int ypos, tipocasilla **matrix){

	matrix[xpos][ypos].estado = 1;
}

void marcador(int posx, int posy, tipocasilla **matrix){

	matrix[posx][posy].etiqueta = '$';
}

void imprimir(int filas, int column,tipocasilla **matrix){

	for (int i = 0; i < filas; i++){
		for (int j = 0; j < column; j++){
			if(matrix[i][j].estado==0)
				printf("|%c|",matrix[i][j].etiqueta);
			if(matrix[i][j].estado==1)	
				printf("|%d|",matrix[i][j].nminas);	
		}
	printf("\n");
	}

}
void jugar(int filas, int column,int minas, tipocasilla **matrix){
	int f=0;
	int xpos;
	int ypos;
	char comando;
	while(f==0){
		printf("Digite el comando: o para abrir y f para asignar bandera\n");
		scanf("%s",&comando);
		printf("Digite la primera coordenada\n");
		scanf("%d",&ypos);
		printf("Digite la segunda coordenada\n");
		scanf("%d",&xpos);

		if(comando=='o'){
			if(matrix[xpos][ypos].tipo =='m'){
				f=10;
				printf("Perdiste Fin Del Juego\n");
			}
			if(matrix[xpos][ypos].tipo =='c'){
				abrir(xpos,ypos,matrix);
				abrircercano(matrix,xpos,ypos,minas,column,filas);
				f=0;
				imprimir(filas,column,matrix);
			}
		}
		if(comando=='f'){
			marcador(xpos,ypos,matrix);
			imprimir(filas,column,matrix);
		}
	}
}

int main(int argc, char *argv[]){
	int filas = atoi(argv[1]);
	int column = atoi(argv[2]);
	int minas = atoi(argv[3]);
	tipocasilla **matrix;

	matrix = iniciarmatrix(filas,column);
	llenarmatrix(matrix,filas,column,minas);
	llenarminas(matrix,filas,column,minas);
	minascercanas(matrix,filas,column,minas);
	jugar(filas,column,minas,matrix);
}