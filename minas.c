#include <stdio.h>
#include <stdlib.h>
#include <time.h>



void iniciarjuego(int filas, int column, int ** matrix){
	for (int i = 0; i < filas; i++){
		matrix[i]=(int *) malloc (column*sizeof(int ));
	}
	for (int i = 0; i < filas; i++){
		for (int j = 0; j < column; j++){
			matrix[i][j] = 0;
		}
	}
}

void imprimir(int filas, int column, int **matrix){

	for (int i = 0; i < filas; i++){
		for (int j = 0; j < column; j++){
			printf("%d\t",matrix[i][j]);
		}
		printf("\n");
	}
}

void iniciarminas(int filas, int column, int ** minas){
	for (int i = 0; i < filas; i++){
		minas[i]=(int *) malloc (column*sizeof(int ));
	}
	srand(time(NULL));
	for (int i = 0; i < filas; i++){
		for (int j = 0; j < column; j++){
			minas[i][j] = rand()%2;
		}
	}
}

void imprimirminas(int filas, int column, int **minas){
	printf("\n");
	for (int i = 0; i < filas; i++){
		for (int j = 0; j < column; j++){
			printf("%d\t",minas[i][j]);
		}
		printf("\n");
	}
}

void juego(int filas, int column, int **minas){

int *m[2];

scanf("Digite la primera coordenada %d\n",m[0]);
scanf("Digite la segunda coordenada %d\n",m[1]);
}

int main(int argc, char *argv[]){
	int filas = atoi(argv[1]);
	int column = atoi(argv[2]);

	int **matrix;
	matrix = (int **) malloc (filas*sizeof(int *));

	iniciarjuego(filas,column,matrix);
	imprimir(filas,column,matrix);

	int **minas = (int **) malloc (filas*sizeof(int *));
	iniciarminas(filas,column,minas);
	imprimirminas(filas,column,minas);
}